package com.funbiscuit.tutorials.thenewboston.datasource.network.dto

import com.funbiscuit.tutorials.thenewboston.model.Bank

data class BankList(
        val results: Collection<Bank>
)
