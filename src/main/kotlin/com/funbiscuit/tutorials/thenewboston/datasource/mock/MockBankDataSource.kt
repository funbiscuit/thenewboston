package com.funbiscuit.tutorials.thenewboston.datasource.mock

import com.funbiscuit.tutorials.thenewboston.datasource.BankDataSource
import com.funbiscuit.tutorials.thenewboston.model.Bank
import org.springframework.stereotype.Repository
import java.lang.IllegalArgumentException

@Repository("mock")
class MockBankDataSource : BankDataSource {
    val banks = mutableListOf(
            Bank("1234", 3.14, 11),
            Bank("1010", 17.0, 0),
            Bank("5678", 0.0, 100),
    )

    override fun retrieveBanks(): Collection<Bank> = banks
    override fun retrieveBank(accountNumber: String): Bank =
            banks.firstOrNull() { it.accountNumber == accountNumber }
                    ?: throw NoSuchElementException(
                            "Could not find a bank with account number $accountNumber")

    override fun createBank(bank: Bank): Bank {
        if (banks.any { it.accountNumber == bank.accountNumber }) {
            throw IllegalArgumentException(
                    "Bank with account number ${bank.accountNumber} already exists")
        }

        banks.add(bank)
        return bank
    }

    override fun updateBank(bank: Bank): Bank {
        if (!banks.removeIf { it.accountNumber == bank.accountNumber })
            throw NoSuchElementException(
                    "Could not find a bank with account number ${bank.accountNumber}")

        banks.add(bank)
        return bank;
    }

    override fun deleteBank(accountNumber: String) {
        if (!banks.removeIf { it.accountNumber == accountNumber })
            throw NoSuchElementException(
                    "Could not find a bank with account number $accountNumber")
    }
}
