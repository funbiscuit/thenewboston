package com.funbiscuit.tutorials.thenewboston.controller

import com.fasterxml.jackson.databind.ObjectMapper
import com.funbiscuit.tutorials.thenewboston.model.Bank
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.web.servlet.*

@SpringBootTest
@AutoConfigureMockMvc
internal class BankControllerTest @Autowired constructor(
        private val mockMvc: MockMvc,
        private val objectMapper: ObjectMapper
) {

    val baseUrl = "/api/banks"

    @Nested
    @DisplayName("GET /api/banks")
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    inner class GetBanks {

        @Test
        fun `should return all banks`() {
            // when/then
            mockMvc.get(baseUrl)
                    .andDo { print() }
                    .andExpect {
                        status { isOk() }
                        content { contentType(MediaType.APPLICATION_JSON) }
                        jsonPath("$[0].account_number") {
                            value("1234")
                        }
                    }
        }
    }

    @Nested
    @DisplayName("GET /api/banks/{accountNumber}")
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    inner class GetBank {

        @Test
        fun `should return the bank with given account number`() {
            // given
            val accountNumber = 1234

            // when/then
            mockMvc.get("$baseUrl/$accountNumber")
                    .andDo { print() }
                    .andExpect {
                        status { isOk() }
                        content { contentType(MediaType.APPLICATION_JSON) }
                        jsonPath("$.trust") {
                            value("3.14")
                        }
                        jsonPath("$.default_transaction_fee") {
                            value("11")
                        }
                    }

        }

        @Test
        fun `should return NOT FOUND if account number does not exist`() {
            // given
            val accountNumber = "something"

            // when/then
            mockMvc.get("$baseUrl/$accountNumber")
                    .andDo { print() }
                    .andExpect {
                        status { isNotFound() }
                    }

        }
    }

    @Nested
    @DisplayName("POST /api/banks")
    @DirtiesContext
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    inner class PostNewBank {
        @Test
        fun `should add the new bank`() {
            // given
            val newBank = Bank("acc123", 31.415, 2)

            // when
            val performPost = mockMvc.post(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(newBank)
            }

            // then
            performPost.andDo { print() }
                    .andExpect {
                        status { isCreated() }
                        content {
                            contentType(MediaType.APPLICATION_JSON)
                            json(objectMapper.writeValueAsString(newBank))
                        }
                    }

            mockMvc.get("$baseUrl/${newBank.accountNumber}")
                    .andExpect {
                        content {
                            json(objectMapper.writeValueAsString(newBank))
                        }
                    }

        }

        @Test
        fun `should return BAD REQUEST if bank with given account number already exists`() {
            // given
            val newBank = Bank("1234", 31.415, 2)


            // when
            val performPost = mockMvc.post(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(newBank)
            }

            // then
            performPost.andDo { print() }
                    .andExpect {
                        status { isBadRequest() }
                    }
        }

    }

    @Nested
    @DisplayName("PATCH /api/banks")
    @DirtiesContext
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    inner class PatchBank {
        @Test
        fun `should update an existing bank`() {
            // given
            val updatedBank = Bank("1234", 10.0, 1)

            // when
            val performPatch = mockMvc.patch(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(updatedBank)
            }

            // then
            performPatch.andDo { print() }
                    .andExpect {
                        status { isOk() }
                        content {
                            contentType(MediaType.APPLICATION_JSON)
                            json(objectMapper.writeValueAsString(updatedBank))
                        }
                    }

            mockMvc.get("$baseUrl/${updatedBank.accountNumber}")
                    .andExpect {
                        content {
                            json(objectMapper.writeValueAsString(updatedBank))
                        }
                    }
        }

        @Test
        fun `should return NOT FOUND if no bank with given account exists`() {
            // given
            val updatedBank = Bank("bad", 10.0, 1)

            // when
            val performPatch = mockMvc.patch(baseUrl) {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsString(updatedBank)
            }

            // then
            performPatch.andDo { print() }
                    .andExpect {
                        status { isNotFound() }
                    }
        }
    }

    @Nested
    @DisplayName("DELETE /api/banks/{accountNumber}")
    @DirtiesContext
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    inner class DeleteBank {
        @Test
        fun `should delete bank with given account number`() {
            // given
            val accountNumber = 1234

            // when
            mockMvc.delete("$baseUrl/$accountNumber")
                    .andDo { print() }
                    .andExpect {
                        status { isNoContent() }
                    }

            // then
            mockMvc.get("$baseUrl/$accountNumber")
                    .andExpect { status { isNotFound() } }
        }

        @Test
        fun `should return NOT FOUND when bank with account number not found`() {
            // given
            val accountNumber = "bad"

            // when/then
            mockMvc.delete("$baseUrl/$accountNumber")
                    .andDo { print() }
                    .andExpect { status { isNotFound() } }

        }
    }
}